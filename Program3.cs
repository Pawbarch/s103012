﻿using System;

namespace Zadanie3
{
    class odkurzacz
    {

        static void Uduszeniesiekablem()
        {
            Console.WriteLine("Owin kabel wokol szyi");
        }
        static void MetodaOdkurzacz()
        {
            string atrybut1 = "Czystosc";
            string atrybut2 = "Zależny od prądu";
            string atrybut3 = "Ruchomy";
            Console.WriteLine(atrybut1);
            Console.WriteLine(atrybut2);
            Console.WriteLine(atrybut3);
        }
        class zmywarka
        {
            static void Utopieniesiewzmywarce()
            {
                Console.WriteLine("Wejdz do zmywarki i ja wlacz");
            }
            static void MetodaZmywarka()
            {
                string atrybut1 = "Mycie naczyn ";
                string atrybut2 = "Mycie sztucow ";
                string atrybut3 = "Mycie kubkow ";
                Console.WriteLine(atrybut3 + atrybut1 + atrybut2);
            }
            class mikser
            {
                static void Urwijrekemikserem()
                {
                    Console.WriteLine("Wymaga troche staran ale na pewno sie da");
                }
                static void MetodaMikser()
                {
                    string atrybut1 = "Blendowanie owocow ";
                    string atrybut2 = "Bicie piany ";
                    string atrybut3 = "Mieszanie skladnikow ";
                    Console.WriteLine(atrybut3 + atrybut1 + atrybut2);
                }


                static void Main(string[] args)
                {
                
                    MetodaOdkurzacz(); 
                    MetodaZmywarka();
                    MetodaMikser();
                }
            }
        }
    }
} 
